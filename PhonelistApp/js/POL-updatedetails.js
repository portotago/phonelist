﻿// -----------------------------------------------------------------------------------------
// --------------------------------------ABOUT ---------------------------------------------
// Display of Private Tour Bookings

// -----------------------------------------------------------------------------------------
// ------------------------------------- CONFIG --------------------------------------------
var submissionURL = "data/setcontact.aspx";                 // Relative URL for submitting update
var getUsersURL = "data/getcontacts.aspx?filter=all";       // Relative URL for contacts JSON
var screenSwapDelay = 350;
var redirectURL = "updatedetails.aspx";


var statusFlashDurationMS = 2000;   // how many miliseconds status messages visible for

// --------------------------------------------------------------------------------------------
// -------------------- Initial fetch of users list ------------------------------------
var jsonUsersCache = fetchUsers();
var arrUsersCache = JSON.parse(JSON.stringify(jsonUsersCache));

// ------------- set up autocomplete ------------------------
var UserAutoComplete = $.map(jsonUsersCache, function (row) {
    return {
        value: row["searchable"]
    };
});

// ------------------------------------------------------------------------------------
// ------------------------------- FUNCTIONS ------------------------------------------

function StatusFlash(message) {
    $("#StatusContainer").show();
    $("#StatusBar").html(message);
    $("#StatusBar").hide().fadeIn(750).delay(statusFlashDurationMS).fadeOut(750).hide(function () {
        $("#StatusContainer").hide();
    });
}  // end StatusFlash

function fetchUsers() {
    console.log("Fetching users...");
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': getUsersURL + "&random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            json = data;
            //console.log(JSON.stringify(json));
        },
        'error': function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            alert(msg);
        }
    });
    return json;
}  // end function fetchUsers


function imgError(image) {
    image.onerror = "";
    image.src = staffPhotosFolder + "noimage.jpg";
    return true;
} // function imgError


function populateFormDetails(term) {
    // filter JSON by search term
    var result = $.grep(arrUsersCache, function (e) {
        return e.searchable == term;
    });

    // hide fields and set readonly based on whether a contact or a user
    if (result[0].objectClass == "user") {
        // is a user, so suppress:

    } else {
        // assume its a contact
        $('#employeenumber,#office').prop("readonly", true).parent().hide();
        // todo: tweak the label for Title
    }

    // populate form and other GUI values
    $('#displayname').text(result[0].fullname ? result[0].fullname : 'NO FULLNAME FOUND');

    $('#objectguid').text(result[0].objectGUID ? result[0].objectGUID : 'NO OBJECTGUID');
    $('#objectclass').text(result[0].objectClass ? result[0].objectClass : 'NO OBJECTCLASS');
    $('#samaccountname').text(result[0].username ? result[0].username : 'NO SAMACCOUNTNAME');
    $('#ou').text(result[0].OU ? result[0].OU : 'NO SAMACCOUNTNAME');
    $('#email').text(result[0].email ? result[0].email : 'NO EMAIL');

    // Organisational details
    $('#employeenumber').val(result[0].employeenumber ? result[0].employeenumber : '');
    $('#department').val(result[0].department ? result[0].department : '');
    $('#title').val(result[0].title ? result[0].title : '');
    $('#office').val(result[0].office ? result[0].office : '');
    $('#company').val(result[0].company ? result[0].company : '');
    $('#description').val(result[0].description ? result[0].description : '');

    // Contact details
    $('#extension').val(result[0].phoneextension ? result[0].phoneextension : '');
    $('#ddi').val(result[0].phoneddi ? result[0].phoneddi : '');
    $('#officephone').val(result[0].phoneoffice ? result[0].phoneoffice : '');
    $('#homephone').val(result[0].phonehome ? result[0].phonehome : '');
    $('#mobilenumber1').val(result[0].phonemobile1 ? result[0].phonemobile1 : '');
    $('#mobilenumber2').val(result[0].phonemobile2 ? result[0].phonemobile2 : '');
    $('#speeddial1').val(result[0].phonespeeddial1 ? result[0].phonespeeddial1 : '');
    $('#speeddial2').val(result[0].phonespeeddial2 ? result[0].phonespeeddial2 : '');

    // visibility selector
    console.log(result[0].fullname + " visibility = " + result[0].visibility.toLowerCase());
    $("#visibility option[value='" + result[0].visibility.toLowerCase() + "']").attr('selected', 'selected');

    // Finally show the form
    $('#modifyDetails').show(screenSwapDelay);
    $('#findUser').hide(screenSwapDelay);

} // function populateFormDetails


function submitUpdate() {
    console.log('Posting data...');
    var posting = $.post(submissionURL, {
        objectguid: $('#objectguid').text(),
        objectclass: $('#objectclass').text(),
        name: $('#displayname').text(),
        employeenumber: $('#employeenumber').val(),
        department: $('#department').val(),
        title:  $('#title').val(),
        office: $('#office').val(),
        company: $('#company').val(),
        extension: $('#extension').val(),
        ddi: $('#ddi').val(),
        officephone: $('#officephone').val(),
        homephone: $('#homephone').val(),
        mobilenumber1: $('#mobilenumber1').val(),
        mobilenumber2: $('#mobilenumber2').val(),
        speeddial1: $('#speeddial1').val(),
        speeddial2: $('#speeddial2').val(),
        visibility: $('#visibility').val()
        });
    posting.done(function (data) {
        window.location = redirectURL + "?msgtype=Success&msgtext=Submitted update for " + $('#displayname').text() + " - changes are processed overnight";
    });
    posting.fail(function () {
        //failure of posting to database
        alert("Error: Sorry I couldn't submit the update, please contact POL IT!");
    });
} // end function submitUpdate



// ------------------------------------------------------------------------------------
// ------------------------------- GUI HANDLERS ---------------------------------------

// ----------------------------------------------
// ----------- Search box autocomplete ----------
$("#UserSearchBox").autocomplete({
    open: function (event, ui) {
        $('.ui-autocomplete').off('menufocus hover mouseover mouseenter'); // fix/hack for iOS bug where user would have to double tap on list
    },
    appendTo: '#autoCompletePlaceholder',
    source: UserAutoComplete,
    minLength: 3,
    close: function (a, b) {
        // dropdown has closed, but nothing selected... do nothing
    },
    select: function (a, b) {
        populateFormDetails(b.item.value);
        $("#UserSearchBox").val('');
        $("#UserSearchBox").blur();
    },
    response: function (event, ui) {
        // this catches when there is only one possible option
        if (ui.content.length == 1) {
            ui.item = ui.content[0];
            $(this).autocomplete('close');
            console.log('ui.item.value = ' + ui.item.value);

            // populate form with this users details
            populateFormDetails(ui.item.value);

            $("#UserSearchBox").val('');
            $("#UserSearchBox").blur();
        }
    }
});


$("#toggleITDetails").click(function () {
    $("#ITDetails").toggle();
});

$('#cancel').click(function () {
    window.location = window.location.href.split("?")[0];

    // Clear all text inputs
//    $('input:text').val('');
    // Remove modified classes
//    $('div').removeClass('modified');

    // reset visibility selector
//    $("#visibility option:selected").removeAttr("selected");

    // show potentially hidden fields
 //   $('#employeenumber,#office,#extension,#ddi').prop("readonly", false).parent().show();

    // Show the search box, hide the form
 //   $('#findUser').show(screenSwapDelay);
 //   $('#UserSearchBox').focus();
 //   $('#modifyDetails').hide(screenSwapDelay);
 //   $('#ITDetails').hide();
});


$('#submit').click(function () {
    submitUpdate();
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

// ------------------------------------------------------------------------------------
// --------------------------------- EXECUTION ----------------------------------------


$(document).ready(function () {
    var statusMessage = getParameterByName("msgtext");
    if (statusMessage != "" && statusMessage !== null) {
        StatusFlash(statusMessage);
    }

    $('#modifyDetails').hide();

    $('#UserSearchBox').focus();

    $('input').change(function () {
        //todo validate
        $(this).parent().addClass('modified');
    });

});
