﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PhonelistApp.data
{
    public class Contact
    {
        public string contactId;
        public string objectGUID;
        public string objectClass;
        public string username;
        public string surname;
        public string firstname;
        public string fullname;
        public string searchable;
        public string company;
        public string office;
        public string department;
        public string title;
        public string description;
        public string email;

        public string phoneoffice;
        public string phoneddi;
        public string phoneextension;
        public string phonehome;

        public string phonemobile1;
        public string phonemobile2;
        public string phonespeeddial1;
        public string phonespeeddial2;
        public string OU;
        public string visibility;

        public Contact
        (
            string contactId,
            string objectGUID,
            string objectClass,
            string username,
            string surname,
            string firstname,
            string fullname,
            string searchable,
            string company,
            string office,
            string department,
            string title,
            string description,
            string email,

            string phoneoffice,
            string phoneddi,
            string phoneextension,
            string phonehome,

            string phonemobile1,
            string phonemobile2,
            string phonespeeddial1,
            string phonespeeddial2,
            string OU,
            string visibility
        )
        {
            this.contactId = contactId;
            this.objectGUID = objectGUID;
            this.objectClass = objectClass;
            this.username = username;
            this.surname = surname;
            this.firstname = firstname;
            this.fullname = fullname;
            this.searchable = searchable;
            this.company = company;
            this.office = office;
            this.department = department;
            this.title = title;
            this.description = description;
            this.email = email;

            this.phoneoffice = phoneoffice;
            this.phoneddi = phoneddi;
            this.phoneextension = phoneextension;
            this.phonehome = phonehome;

            this.phonemobile1 = phonemobile1;
            this.phonemobile2 = phonemobile2;
            this.phonespeeddial1 = phonespeeddial1;
            this.phonespeeddial2 = phonespeeddial2;
            this.OU = OU;
            this.visibility = visibility;
        }
    }  // end class Contact

    public class phonelist
    {
        public static string LookupContacts(string filter)
        {
            var log = log4net.LogManager.GetLogger("why?");

            // Connect to the DB
            string _connection_string = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (SqlConnection _conn = new SqlConnection(_connection_string))
            {
                _conn.Open();

                SqlCommand cmd = new SqlCommand();

                // get full list of contacts, or limited phonelist contacts (i.e. only users with phone numbers)
                if (filter=="all")
                {
                    cmd = new SqlCommand("dbo.spGetAllContacts", _conn);
                }else {
                    cmd = new SqlCommand("dbo.spGetPhonelist", _conn);
                }


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);

                Contact[] results = new Contact[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // from phoneoffice, phoneextension, determine phoneddi and phoneoffice
                    // if is a user, and phoneoffice has a comma, then phoneoffice = blank, phoneddi=blank (i.e. staff member only has an extension)
                    string phoneOfficeDerived = "";
                    string phoneDDIDerived = "";
                    string searchableField = "";
                    if (dt.Rows[i]["objectClass"].ToString().ToLower() == "user" && dt.Rows[i]["phoneoffice"].ToString().Contains(","))
                    {
                        phoneOfficeDerived = "";
                        phoneDDIDerived = "";
                    }
                    else
                    {
                        phoneOfficeDerived = "";
                        phoneDDIDerived = dt.Rows[i]["phoneoffice"].ToString();
                    }

                    // Compile the "searchable" field used by the autocomplete
                    if(dt.Rows[i]["objectClass"].ToString() == "user") {
                        searchableField = dt.Rows[i]["fullname"].ToString() + " - " + dt.Rows[i]["department"].ToString() + " - " + dt.Rows[i]["title"].ToString();
                    } else {
                        searchableField = dt.Rows[i]["fullname"].ToString() + " - " + dt.Rows[i]["company"].ToString();
                    }

                    results[i] = new Contact
                    (
                        dt.Rows[i]["contactId"].ToString(),
                        dt.Rows[i]["objectGUID"].ToString(),
                        dt.Rows[i]["objectClass"].ToString(),
                        dt.Rows[i]["username"].ToString(),
                        dt.Rows[i]["surname"].ToString(),
                        dt.Rows[i]["firstname"].ToString(),
                        dt.Rows[i]["fullname"].ToString(),
                        searchableField,
                        dt.Rows[i]["company"].ToString(),
                        dt.Rows[i]["office"].ToString(),
                        dt.Rows[i]["department"].ToString(),
                        dt.Rows[i]["title"].ToString(),
                        dt.Rows[i]["description"].ToString(),
                        dt.Rows[i]["email"].ToString(),
                        phoneOfficeDerived,
                        phoneDDIDerived,
                        dt.Rows[i]["phoneextension"].ToString(),
                        dt.Rows[i]["phonehome"].ToString(),
                        dt.Rows[i]["phonemobile1"].ToString(),
                        dt.Rows[i]["phonemobile2"].ToString(),
                        dt.Rows[i]["phonespeeddial1"].ToString(),
                        dt.Rows[i]["phonespeeddial2"].ToString(),
                        dt.Rows[i]["OU"].ToString(),
                        dt.Rows[i]["visibility"].ToString()
                    );
                }

                // Return the JSon Array to the calling script
                return JsonConvert.SerializeObject(results);
            }
        } // end LookupContacts



    }
}