﻿// ---------- DEPENDENCIES---------------
// Note: This solution requires JSON.net which is currently not installable via NuGet package manager, so manually add a reference from another solution.

using log4net;
using log4net.Config;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhonelistApp.data
{
    public partial class bgp_loadcontacts : System.Web.UI.Page
    {

        public static string ConvertDataTableToString(DataTable dataTable)
        {
            var output = new StringBuilder();

            var columnsWidths = new int[dataTable.Columns.Count];

            // Get column widths
            foreach (DataRow row in dataTable.Rows)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var length = row[i].ToString().Length;
                    if (columnsWidths[i] < length)
                        columnsWidths[i] = length;
                }
            }

            // Get Column Titles
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var length = dataTable.Columns[i].ColumnName.Length;
                if (columnsWidths[i] < length)
                    columnsWidths[i] = length;
            }

            // Write Column titles
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var text = dataTable.Columns[i].ColumnName;
                output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
            }
            output.Append("|\n" + new string('=', output.Length) + "\n");

            // Write Rows
            foreach (DataRow row in dataTable.Rows)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var text = row[i].ToString();
                    output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                }
                output.Append("|\n");
            }
            return output.ToString();
        }

        private static string PadCenter(string text, int maxLength)
        {
            int diff = maxLength - text.Length;
            return new string(' ', diff / 2) + text + new string(' ', (int)(diff / 2.0 + 0.5));

        }

        public static DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            var log = log4net.LogManager.GetLogger("why");

            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);

                        log.Debug("bgp-loadcontacts.aspx: " + "added column to data table: " + datecolumn.ToString());
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        // Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception Ex)
            {
                // Log failure
                log.Error("Failed to read CSV: " + Ex.Message.ToString());

                // Notify error via email
                try
                {
                    SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"].ToString(), 25);
                    smtpClient.EnableSsl = false;
                    smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                    smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailFromAddress"].ToString(), "Phonelist App");
                    foreach (var address in ConfigurationManager.AppSettings["EmailEscalalationAddresses"].ToString().Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mail.To.Add(address);
                    }
                    mail.Subject = "Failure to load phonelist CSV to SQL";
                    mail.Body = "There was a problem loading the CSV of to SQL!  The following error message may be helpful: " + Ex.Message.ToString();
                    smtpClient.Send(mail);
                    mail.Dispose();
                    // Log success
                    log.Info("Email about CSV loading failure sent");

                }
                catch (Exception EmailEx)
                {
                    // Log failure
                    log.Error("Failed to send email: " + EmailEx.Message.ToString());
                }

            }
            return csvData;
        } // end GetDataTabletFromCSVFile

        static bool loadContactsToSQL(DataTable csvFileData)
        {
            // This function contains hard-coded column mappings - it is not a general purpose function
            var log = log4net.LogManager.GetLogger("why?");

            string _connection_string = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (SqlConnection dbConnection = new SqlConnection(_connection_string))
            {
                dbConnection.Open();

                // First truncate the table
                SqlCommand cmd = new SqlCommand("dbo.spTruncatePhonelist", dbConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader truncated = cmd.ExecuteReader();
                cmd.Dispose();
                truncated.Close();

                // Parse out extension as a separate field from phoneoffice into a new column
                // Add column to datatable
               // System.Data.DataColumn newColumn = new System.Data.DataColumn("phoneextension", typeof(System.String));
               // newColumn.DefaultValue = "";
              //  csvFileData.Columns.Add(newColumn);

                // Suppress main phone number if it is the backdoor with a comma then the extension
                foreach (DataRow dr in csvFileData.Rows) // search whole table
                {
                    if (dr["phoneoffice"].ToString().Contains(','))
                    {
                        dr["phoneoffice"] = "";
                    }
                }

                // Upload CSV Data to table
                try
                {
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                    {
                        // values in this order: datatable-from-csv field name, db field name
                        s.DestinationTableName = "tblPhonelist";
                        s.ColumnMappings.Add("objectguid", "objectguid");
                        s.ColumnMappings.Add("username", "username");
                        s.ColumnMappings.Add("objectClass", "objectclass");
                        s.ColumnMappings.Add("surname", "surname");
                        s.ColumnMappings.Add("firstname", "firstname");
                        s.ColumnMappings.Add("fullname", "fullname");
                        s.ColumnMappings.Add("office", "office");
                        s.ColumnMappings.Add("company", "company");
                        s.ColumnMappings.Add("department", "department");
                        s.ColumnMappings.Add("description", "description");
                        s.ColumnMappings.Add("employeenumber", "employeenumber");
                        s.ColumnMappings.Add("title", "title");
                        s.ColumnMappings.Add("email", "email");
                        s.ColumnMappings.Add("phonemobile1", "phonemobile1");
                        s.ColumnMappings.Add("phonemobile2", "phonemobile2");
                        s.ColumnMappings.Add("phoneoffice", "phoneoffice");
                        s.ColumnMappings.Add("phoneextension", "phoneextension");
                        s.ColumnMappings.Add("phonehome", "phonehome");
                        s.ColumnMappings.Add("phonespeeddial1", "phonespeeddial1");
                        s.ColumnMappings.Add("phonespeeddial2", "phonespeeddial2");
                        s.ColumnMappings.Add("OU", "OU");
                        s.ColumnMappings.Add("visibility", "visibility");
                        s.WriteToServer(csvFileData);
                    }
                    return true;
                }
                catch (Exception Ex)
                {
                    log.Error("bgp-loadcontacts.aspx: " + Ex.Message.ToString());
                    // Notify error via email
                    try
                    {
                        SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"].ToString(), 25);
                        smtpClient.EnableSsl = false;
                        smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                        smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        MailMessage mail = new MailMessage();
                        mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailFromAddress"].ToString(), "Phonelist App");
                        foreach (var address in ConfigurationManager.AppSettings["EmailEscalalationAddresses"].ToString().Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            mail.To.Add(address);
                        }
                        mail.Subject = "Failure to load phonelist CSV to SQL";
                        mail.Body = "There was a problem loading the CSV of phonelist contacts to SQL!  The following error message may be helpful: " + Ex.Message.ToString();
                        smtpClient.Send(mail);
                        mail.Dispose();
                        // Log success sendng email notifiation
                        log.Info("Email notification of failure to load CSV has been sent to " + ConfigurationManager.AppSettings["EmailEscalalationAddresses"].ToString());
                    }
                    catch (Exception EmailEx)
                    {
                        // Log failure to send failure notification email... lol
                        log.Error("bgp-loadcontacts.aspx: " + "Failed to send failure notification email: " + EmailEx.Message.ToString());
                    }
                    return false;
                }
            }
        } // end loadContactsToSQL

        public static bool loadCSV() {

            var log = log4net.LogManager.GetLogger("why");

            FileInfo myFile = new FileInfo(ConfigurationManager.AppSettings["CSVPathADContacts"].ToString());

            if (myFile != null)
            {
                log.Info("bgp-loadcontacts.aspx: " + "Attempting to load CSV to SQL");
                log.Info("bgp-loadcontacts.aspx: " + "CSV: " + myFile.FullName);

                // Load the target CSV to DB
                DataTable contactsData = GetDataTabletFromCSVFile(myFile.FullName);
                //log.Debug(ConvertDataTableToString(contactsData));


                bool CSVLoadResult = loadContactsToSQL(contactsData);
                if (CSVLoadResult)
                {
                    // Load was successful, log
                    log.Info("bgp-loadcontacts.aspx: " + "Contacts successfully loaded from CSV to SQL table.");
                    myFile.Delete();

                    string path = ConfigurationManager.AppSettings["TouchFilePath"].ToString();
                    if (!File.Exists(path))
                    {
                        File.Create(path);
                    }
                    else
                    {
                        // Take an action that will affect the write time.
                        File.WriteAllText(path, DateTime.Now.ToString());
                        //File.SetLastWriteTime(path, DateTime.Now);
                    }

                    return true;
                }
                else
                {
                    // Load was UNsuccessful, now relocate CSV
                    log.Error("bgp-loadcontacts.aspx: " + "Contacts failed to load from CSV.");
                    //TODO: send escalation email
                    return false;
                }   
            }
            else
            {
                // CSV file not found
                log.Error("bgp-loadcontacts.aspx: " + "CSV file not found at path: " + ConfigurationManager.AppSettings["CSVPathADContacts"].ToString());
                // TODO: send escalation email
                return false;
            }
        } // end loadCSV

        protected void Page_Load(object sender, EventArgs e)
        {
            // call the main function
            bool successfulLoadToSQL = loadCSV();
            // Return some basic JSON
            if (successfulLoadToSQL)
            {
                Response.Write("{\"contactsloaded\":\"true\"}");

                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"].ToString(), 25);
                smtpClient.EnableSsl = false;
                smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailFromAddress"].ToString(), "Phonelist App");
                foreach (var address in ConfigurationManager.AppSettings["EmailEscalalationAddresses"].ToString().Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                mail.Subject = "AD Contacts Loaded to Phonelist";
                mail.Body = "The CSV of phonelist contacts from AD has been successfully loaded to the database.\nCSV path: " + ConfigurationManager.AppSettings["CSVPathADContacts"].ToString() + "\nDB: PONS > CorporateDB > tblPhonelist.\nThe CSV file was then deleted.";
                smtpClient.Send(mail);
                mail.Dispose();
            }
            else
            {
                Response.Write("{\"contactsloaded\":\"false\"}");
            }
        }
    } // end Page_Load
}