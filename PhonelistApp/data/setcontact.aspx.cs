﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace PhonelistApp.data
{
    public partial class setcontact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());

            var context = HttpContext.Current;

            // Declare inputs
            string objectguid = "";
            string objectclass = "";
            string name = "";
            string employeenumber = "";
            string description = "";
            string title = "";
            string department = "";
            string office = "";
            string company = "";
            string extension = "";
            string ddi = "";
            string officephone = "";
            string homephone = "";
            string mobilenumber1 = "";
            string mobilenumber2 = "";
            string speeddial1 = "";
            string speeddial2 = "";
            string visibility = "";
            string updateRequester = "";

            // Sanitize input 
            objectguid = context.Request["objectguid"];
            objectclass = context.Request["objectclass"];
            name = context.Request["name"];
            employeenumber = context.Request["employeenumber"];
            title = context.Request["title"];
            description = context.Request["description"];
            department = context.Request["department"];
            office = context.Request["office"];
            company = context.Request["company"];
            extension = context.Request["extension"];
            ddi = context.Request["ddi"];
            officephone = context.Request["officephone"];
            homephone = context.Request["homephone"];
            mobilenumber1 = context.Request["mobilenumber1"];
            mobilenumber2 = context.Request["mobilenumber2"];
            speeddial1 = context.Request["speeddial1"];
            speeddial2 = context.Request["speeddial2"];
            visibility = context.Request["visibility"];
            updateRequester = Context.User.Identity.Name;
           
            // if has no DDI but has an extension, set officephone to be POL Backdoor no + comma then extension
            if (ddi == "" && officephone == "" && extension != "")
            {
                officephone = "03 472 9709, " + extension;
            }
            else if (ddi != "" && officephone == "")
            {
                officephone = ddi;
            }
            else { 
                // just leave office phone as whatever was passed in
            }

            log.Debug("setcontact.aspx.cs: " + "ddi: " + ddi + ", officephone:" + officephone + ", extension: " + extension);
 

            // sanitize user name field for use in filename
            string sanitisedname = "";
            Regex rgx = new Regex("[^a-zA-Z0-9-]");
            sanitisedname = rgx.Replace(name, "");

            // Generate CSV file for import into AD
            string CSVPath = ConfigurationManager.AppSettings["TemporaryFileStore"].ToString() + sanitisedname + "_" + objectclass.ToUpper() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            using (StreamWriter sw = File.CreateText(CSVPath))
            {
                // prepare strings
                string csvColumnHeaderLine = "";
                string csvDataLine = "";
                csvColumnHeaderLine = @"objectguid,Name,Title,Description,Office,Company,Department,EmployeeNumber,PhoneExtension,PhoneOffice,PhoneHome,PhoneMobile1,PhoneMobile2,PhoneSpeedDial1,PhoneSpeedDial2,Visibility,UpdateRequester";
                csvDataLine += "\"" + objectguid + "\",";
                csvDataLine += "\"" + name + "\",";
                csvDataLine += "\"" + title + "\",";
                csvDataLine += "\"" + description + "\",";
                csvDataLine += "\"" + office + "\",";
                csvDataLine += "\"" + company + "\",";
                csvDataLine += "\"" + department + "\",";
                csvDataLine += "\"" + employeenumber + "\",";
                csvDataLine += "\"" + extension + "\",";
                csvDataLine += "\"" + officephone + "\",";
                csvDataLine += "\"" + homephone + "\","; 
                csvDataLine += "\"" + mobilenumber1 + "\",";
                csvDataLine += "\"" + mobilenumber2 + "\",";
                csvDataLine += "\"" + speeddial1 + "\",";
                csvDataLine += "\"" + speeddial2 + "\",";
                csvDataLine += "\"" + visibility + "\",";
                csvDataLine += "\"" + updateRequester + "\"";
                // write to CSV
                sw.WriteLine(csvColumnHeaderLine);
                sw.WriteLine(csvDataLine);

                log.Info("setcontact.aspx.cs: " + "CSV File generated: " + CSVPath);
            }
        }
    }
}