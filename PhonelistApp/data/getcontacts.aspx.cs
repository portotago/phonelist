﻿using log4net;
using log4net.Config;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace PhonelistApp.data
{
    public partial class getcontacts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // get filter from the querystring
            var context = HttpContext.Current;
            string filter = "";
            filter = context.Request["filter"];
            if (filter == "all")
            {
                Response.Write(phonelist.LookupContacts("all"));
            } else {
                Response.Write(phonelist.LookupContacts("phonelist"));
            }
        }
    }
}