﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="updatedetails.aspx.cs" Inherits="PhonelistApp.updatedetails" %>

<html>
<head runat="server">
    <title>Update details</title>
    
    <!-- libraries and plugins -->
    <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.v1.12.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>


    <!-- stylesheets -->
    <link rel="stylesheet" type="text/css" href="css/POL-updatedetails.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css"/>

    <!-- flags -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />

    <!-- mobile web app flags -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="Private Tours Schedule" />

    <!-- icons -->
    <link rel="icon" sizes="128x128" href="images/launch-icon-128.png" />
    <link rel="apple-touch-startup-image" href="images/launch-icon-128.png" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/launch-icon-128.png" />
    
    <!-- fix poopy IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,IE=11,IE=10,IE=9,IE=8" />
</head>
<body>

    <div id="StatusContainer"><div id="StatusBar"></div></div>

    <div id="main">
    
        <div id="findUser">
            <h1>Update account details in AD</h1>
            <div><span class="fieldTitle">Search for account</span><input type="text" id="UserSearchBox" /><span></span></div>
        </div>
        <div id="modifyDetails">
            <h1>Update AD account details for <span id="displayname"></span></h1>
            <div id="itcontext">
                <div id="toggleITDetails">SHOW/HIDE IT STUFF</div>
                <div id="ITDetails">
                    <ul>
                        <li><b>SAMACCOUNTNAME:</b> <span id="samaccountname"></span></li>
                        <li><b>Email:</b> <span id="email"></span></li>
                        <li><b>ObjectClass:</b> <span id="objectclass"></span></li>
                        <li><b>OU:</b> <span id="ou"></span></li>
                        <li><b>ObjectGUID:</b> <span id="objectguid"></span></li>
                    </ul>
                </div>
            </div>
            <form class="cmxform" id="commentForm" method="post" action="">
            <div class="formSection">
                <h2>Organisational details</h2>
                <div><span class="fieldTitle">Employee number</span><input type="text" id="employeenumber" name="employeenumber" class="validEmployeeNumber" autocomplete="off" /><label for="employeenumber"></label></div>
                <div><span class="fieldTitle">Title</span><input type="text" id="title" name="title" autocomplete="off" class="validText"/><label for="title">Should align with Work Area Position (WAP)</label></div>
                <div><span class="fieldTitle">Department</span><input type="text" id="department" name="department" autocomplete="off" class="validText"/><label for="department"></label></div>
                <div><span class="fieldTitle">Office</span><input type="text" id="office" name="office" autocomplete="off" class="validText"/><label for="office"></label></div>
                <div><span class="fieldTitle">Company</span><input type="text" id="company" name="company" autocomplete="off" class="validText"/><label for="company"></label></div>
                <div><span class="fieldTitle">Description</span><input type="text" id="description" name="description" autocomplete="off" class="validText"/><label for="description"></label></div>
            </div>
            <div class="formSection">
                <h2>Contact details</h2>
                <div><span class="fieldTitle">POL Extension</span><input type="text" id="extension" name="extension" autocomplete="off" class="validExtension" /></div>
                <div><span class="fieldTitle">POL DDI</span><input type="text" id="ddi" name="ddi" autocomplete="off" class="validPhoneNumber" /></div>
                <div><span class="fieldTitle">Office phone</span><input type="text" id="officephone" name="officephone" autocomplete="off" class="validPhoneNumber" /></div>
                <div><span class="fieldTitle">Home phone</span><input type="text" id="homephone" name="homephone" autocomplete="off" class="validPhoneNumber" /></div>
                <div><span class="fieldTitle">Mobile number 1</span><input type="text" id="mobilenumber1" name="mobilenumber1" autocomplete="off" class="validPhoneNumber" /></div>
                <div><span class="fieldTitle">Mobile number 2</span><input type="text" id="mobilenumber2" name="mobilenumber2" autocomplete="off" class="validPhoneNumber" /></div>
                <div><span class="fieldTitle">Speed dial 1</span><input type="text" id="speeddial1" name="speeddial1" autocomplete="off" class="validSpeedDial" /><label for="speeddial1">(to mobile number)</label></div>
                <div><span class="fieldTitle">Speed dial 2</span><input type="text" id="speeddial2" name="speeddial2" autocomplete="off" class="validSpeedDial" /><label for="speeddial2">(to landline)</label></div>
            </div>
            <div class="formSection">
                <h2>Visibility</h2>
                <div>
                    <span class="fieldTitle">Visibility:</span><select id="visibility">
                        <option value="">Only visible in AD (Blank)</option>
                        <option value="yes">Show on one-pager phonelist (Yes)</option>
                        <option value="no">Find in lookup only (No)</option>
                    </select>
                </div>
            </div>
            <div class="formSection">
                <h2>Save</h2>
                <input type="submit" value="cancel" id="cancel" />
                <input type="submit" value="commit changes" id="submit" />
            </div>
            </form>
        </div><!-- end #modifyDetails -->
    </div><!-- end #main -->
</body>

<script src="js/POL-updatedetails.js" type="text/javascript"></script>
<script> 
    // configure validation behaviours
    $("#commentForm").validate({
        highlight: function (element) {
            $(element).parent('div').addClass('error');
            $('#submit').attr('disabled', 'disabled');
        },
        unhighlight: function (element) {
            $(element).parent('div').removeClass('error');
        },
        success: function(label) {
            $('#submit').removeAttr('disabled');
        }
    });


   

    // rules and messages based on input class value
        $('.validText').each(function () {
        $(this).rules('add', {
            pattern: /^[A-Za-z0-9 ()\-_&.,!"'/]*$/,
            messages: {
                pattern: "only simple punctuation allowed"
            }
        });
    });


    $('.validPhoneNumber').each(function () {
        $(this).rules('add', {
            pattern: /^[\d\s()+-]+$/,
            minlength: 7,
            messages: {
                pattern: "only numbers and spaces allowed",
                minlength: "looks to short to be a phone number"
            }
        });
    });

    $('.validEmployeeNumber').each(function () {
        $(this).rules('add', {
            pattern: /^[\d]*$/,
            maxlength:5,
            messages: {
                pattern: "only numbers allowed",
                maxlength: "can't be more than 4 digits"
            }
        });
    });

    $('.validExtension').each(function () {
        $(this).rules('add', {
            pattern: /^[\d]{4}$/,
            messages: {
                pattern: "must be four numbers"
            }
        });
    });

    $('.validSpeedDial').each(function () {
        $(this).rules('add', {
            pattern: /^[*][\d]{3}$/,
            messages: {
                pattern: "must be star * then three digits e.g. *123"
            }
        });
    });
    </script>
</html>
